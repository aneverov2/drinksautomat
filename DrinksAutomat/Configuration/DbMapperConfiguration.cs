﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DrinksAutomat.DAL.Entities;
using DrinksAutomat.Models;

namespace DrinksAutomat.Configuration
{
    public class DbMapperConfiguration : Profile
    {
        public DbMapperConfiguration()
        {
            CreateMap<Coin, CoinModel>();
            CreateMap<CoinModel, Coin>();
                //.ForMember(x => x.Automat, s => s.Ignore());

            CreateMap<Drink, DrinkModel>()
                .ForMember(x => x.ImagePath, s => s.Ignore())
                .ForMember(x => x.Image, opt => opt.MapFrom(s => s.ImagePath));
            CreateMap<DrinkModel, Drink>()
                .ForMember(x => x.Quantity, s => s.Ignore())
                //.ForMember(x => x.Automat, s => s.Ignore())
                .ForMember(x => x.ImagePath, s => s.Ignore());

           
            //CreateMap<SysTransaction, SysTransactionDTO>()
            //    .ForMember(x => x.ClientFromName, opt => opt.MapFrom(s => s.ClientFrom.LastName))
            //    .ForMember(x => x.ClientToName, opt => opt.MapFrom(s => s.ClientTo.LastName))
            //    .ForMember(x => x.AgentFromName, opt => opt.MapFrom(s => s.AgentFrom.Name))
            //    .ForMember(x => x.AgentToName, opt => opt.MapFrom(s => s.AgentTo.Name))
            //    .ForMember(x => x.CountryName, opt => opt.MapFrom(s => s.Country.Name))
            //    .ForMember(x => x.CurrencyName, opt => opt.MapFrom(s => s.Currency.Name))
            //    .ForMember(x => x.CurrencyId, opt => opt.MapFrom(s => s.Currency.Id))
            //    .ForMember(x => x.AgentFromTextPromo, opt => opt.MapFrom(s => s.AgentFrom.TextPromo))

            //    .ForMember(x => x.AgentFromCommission, opt => opt.MapFrom(s => s.Commissions.
            //        FirstOrDefault(c => c.AgentId == s.AgentFromId && c.TransactionId == s.Id).Value))
            //    .ForMember(x => x.AgentToCommission, opt => opt.MapFrom(s => s.Commissions.
            //        FirstOrDefault(c => c.AgentId == s.AgentToId && c.TransactionId == s.Id).Value))
            //    .ForMember(x => x.SystemCommission, opt => opt.MapFrom(s => s.Commissions
            //        .FirstOrDefault(c => c.Agent.Name == "System" && c.TransactionId == s.Id).Value))

            //CreateMap<Tariff, TariffDTO>()
            //    .ForMember(x => x.AgentName, opt => opt.MapFrom(s => s.Agent.Name))
            //    .ForMember(x => x.CountryName, opt => opt.MapFrom(s => s.Country.Name))
            //    .ForMember(x => x.CurrencyName, opt => opt.MapFrom(s => s.Currency.Name))
            //    .ForMember(x => x.RangeMinValue, opt => opt.MapFrom(s => s.Range.MinValue))
            //    .ForMember(x => x.RangeMaxValue, opt => opt.MapFrom(s => s.Range.MaxValue))
            //    .ForMember(x => x.TypeOfCommission, opt => opt.MapFrom(s => s.CommissionType));
           

            

        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DrinksAutomat.Migrations
{
    public partial class AddAutomatEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Count",
                table: "Drinks",
                newName: "Quantity");

            migrationBuilder.AddColumn<int>(
                name: "AutomatId",
                table: "Drinks",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AutomatId",
                table: "Coins",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Automats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CoinsQty = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Automats", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Drinks_AutomatId",
                table: "Drinks",
                column: "AutomatId");

            migrationBuilder.CreateIndex(
                name: "IX_Coins_AutomatId",
                table: "Coins",
                column: "AutomatId");

            migrationBuilder.AddForeignKey(
                name: "FK_Coins_Automats_AutomatId",
                table: "Coins",
                column: "AutomatId",
                principalTable: "Automats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Drinks_Automats_AutomatId",
                table: "Drinks",
                column: "AutomatId",
                principalTable: "Automats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coins_Automats_AutomatId",
                table: "Coins");

            migrationBuilder.DropForeignKey(
                name: "FK_Drinks_Automats_AutomatId",
                table: "Drinks");

            migrationBuilder.DropTable(
                name: "Automats");

            migrationBuilder.DropIndex(
                name: "IX_Drinks_AutomatId",
                table: "Drinks");

            migrationBuilder.DropIndex(
                name: "IX_Coins_AutomatId",
                table: "Coins");

            migrationBuilder.DropColumn(
                name: "AutomatId",
                table: "Drinks");

            migrationBuilder.DropColumn(
                name: "AutomatId",
                table: "Coins");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                table: "Drinks",
                newName: "Count");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DrinksAutomat.Migrations
{
    public partial class RemoveAutomatEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coins_Automats_AutomatId",
                table: "Coins");

            migrationBuilder.DropForeignKey(
                name: "FK_Drinks_Automats_AutomatId",
                table: "Drinks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Automats",
                table: "Automats");

            migrationBuilder.RenameTable(
                name: "Automats",
                newName: "Automat");

            migrationBuilder.RenameColumn(
                name: "Count",
                table: "Coins",
                newName: "Quantity");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Automat",
                table: "Automat",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Coins_Automat_AutomatId",
                table: "Coins",
                column: "AutomatId",
                principalTable: "Automat",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Drinks_Automat_AutomatId",
                table: "Drinks",
                column: "AutomatId",
                principalTable: "Automat",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coins_Automat_AutomatId",
                table: "Coins");

            migrationBuilder.DropForeignKey(
                name: "FK_Drinks_Automat_AutomatId",
                table: "Drinks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Automat",
                table: "Automat");

            migrationBuilder.RenameTable(
                name: "Automat",
                newName: "Automats");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                table: "Coins",
                newName: "Count");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Automats",
                table: "Automats",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Coins_Automats_AutomatId",
                table: "Coins",
                column: "AutomatId",
                principalTable: "Automats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Drinks_Automats_AutomatId",
                table: "Drinks",
                column: "AutomatId",
                principalTable: "Automats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

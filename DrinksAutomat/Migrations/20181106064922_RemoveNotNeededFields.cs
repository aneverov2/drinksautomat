﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DrinksAutomat.Migrations
{
    public partial class RemoveNotNeededFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coins_Automat_AutomatId",
                table: "Coins");

            migrationBuilder.DropForeignKey(
                name: "FK_Drinks_Automat_AutomatId",
                table: "Drinks");

            migrationBuilder.DropTable(
                name: "Automat");

            migrationBuilder.DropIndex(
                name: "IX_Drinks_AutomatId",
                table: "Drinks");

            migrationBuilder.DropIndex(
                name: "IX_Coins_AutomatId",
                table: "Coins");

            migrationBuilder.DropColumn(
                name: "AutomatId",
                table: "Drinks");

            migrationBuilder.DropColumn(
                name: "AutomatId",
                table: "Coins");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AutomatId",
                table: "Drinks",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AutomatId",
                table: "Coins",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Automat",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CoinsQty = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Automat", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Drinks_AutomatId",
                table: "Drinks",
                column: "AutomatId");

            migrationBuilder.CreateIndex(
                name: "IX_Coins_AutomatId",
                table: "Coins",
                column: "AutomatId");

            migrationBuilder.AddForeignKey(
                name: "FK_Coins_Automat_AutomatId",
                table: "Coins",
                column: "AutomatId",
                principalTable: "Automat",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Drinks_Automat_AutomatId",
                table: "Drinks",
                column: "AutomatId",
                principalTable: "Automat",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

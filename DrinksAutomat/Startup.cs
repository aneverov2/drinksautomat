﻿using DrinksAutomat.BLL.Infrastructure;
using DrinksAutomat.Configuration;
using DrinksAutomat.Data;
using DrinksAutomat.DAL.Entities;
using DrinksAutomat.DAL.Interfaces;
using DrinksAutomat.DAL.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DrinksAutomat
{
    public class Startup
    {
        private string _contentRootPath = "";
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _contentRootPath = env.ContentRootPath;
            AutomapperInit.Initialize();
        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            string conn = Configuration.GetConnectionString("DefaultConnection");
            if (conn.Contains("%CONTENTROOTPATH%"))
            {
                conn = conn.Replace("%CONTENTROOTPATH%", _contentRootPath);
            }
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(
                    conn);
            });
           
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IRepository<Drink>, DrinkRepository>();
            services.AddTransient<IRepository<Coin>, CoinRepository>();
            services.AddTransient<FileUploadService>();
            services.AddTransient<DrinkService>();
            services.AddTransient<CoinService>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

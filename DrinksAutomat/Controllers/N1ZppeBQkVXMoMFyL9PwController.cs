﻿using DrinksAutomat.BLL.Infrastructure;
using DrinksAutomat.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DrinksAutomat.Controllers
{
    public class N1ZppeBQkVXMoMFyL9PwController : Controller
    {
        private DrinkService DrinkService { get; }

        public N1ZppeBQkVXMoMFyL9PwController(DrinkService drinkService)
        {
            DrinkService = drinkService;
        }

        public ActionResult Index()
        {
            return View(DrinkService.GetUserModel());
        }

        public ActionResult LoadDrinks()
        {
            return View(DrinkService.GetUserModel());
        }

        // GET: Drink/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: Drink/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: Drink/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AutomatModel model)
        {
            try
            {
                DrinkService.Create(model.DrinkModel);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Drink/Edit/5
        public string Edit(int drinkId)
        {
            return JsonConvert.SerializeObject(DrinkService.Get(drinkId),
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        // POST: Drink/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AutomatModel model)
        {
            try
            {
                DrinkService.Update(model.DrinkModel);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Drink/Delete/5
        public string Delete(int drinkId)
        {
            return JsonConvert.SerializeObject(DrinkService.Get(drinkId),
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        // POST: Drink/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteItem(AutomatModel model)
        {
            try
            {
                DrinkService.Delete(model.DrinkModel.Id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public string BuyDrink(int drinkId, int temp)
        {
            //string s = JsonConvert.SerializeObject(DrinkService.BuyDrink(drinkId, temp),
            //    new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            return JsonConvert.SerializeObject(DrinkService.BuyDrink(drinkId, temp),
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

    }
}

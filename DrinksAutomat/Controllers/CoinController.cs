﻿using DrinksAutomat.BLL.Infrastructure;
using DrinksAutomat.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DrinksAutomat.Controllers
{
    public class CoinController : Controller
    {
        private CoinService CoinService { get; set; }

        public CoinController(CoinService coinService)
        {
            CoinService = coinService;
        }
        // GET: Coin
        public ActionResult Index()
        {
            //return View(CoinService.GetAllCoins());
            return RedirectToAction("Index", "Drink");
        }

        // POST: Coin/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AutomatModel model)
        {
            try
            {
                CoinService.Create(model.CoinModel);

                return RedirectToAction("Index", "N1ZppeBQkVXMoMFyL9Pw");
            }
            catch
            {
                return View();
            }
        }

        // GET: Coin/Edit/5
        public string Edit(int id)
        {
            if (id == 0)
            {
                return "У вас не прав доступа";
            }
            return JsonConvert.SerializeObject(CoinService.Get(id),
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        // POST: Coin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AutomatModel model)
        {
            try
            {
                CoinService.Update(model.CoinModel);

                return RedirectToAction("Index", "N1ZppeBQkVXMoMFyL9Pw");
            }
            catch
            {
                return View();
            }
        }

        // GET: Coin/Delete/5
        public string Delete(int id)
        {
            if (id == 0)
            {
                return "У вас не прав доступа";
            }
            return JsonConvert.SerializeObject(CoinService.Get(id),
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        // POST: Coin/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteItem(AutomatModel model)
        {
            try
            {
                CoinService.Delete(model.CoinModel.Id);

                return RedirectToAction("Index", "N1ZppeBQkVXMoMFyL9Pw");
            }
            catch
            {
                return View();
            }
        }

        public string GetCoinValue(int value, int temp)
        {
            return JsonConvert.SerializeObject(CoinService.GetCoinValue(value, temp),
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
    }
}
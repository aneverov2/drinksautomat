﻿using DrinksAutomat.BLL.Infrastructure;
using DrinksAutomat.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DrinksAutomat.Controllers
{
    public class DrinkController : Controller
    {
        private DrinkService DrinkService { get; }

        public DrinkController(DrinkService drinkService)
        {
            DrinkService = drinkService;
        }
        // GET: Drink
        public ActionResult Index(string id)
        {
            if (id == "N1ZppeBQkVXMoMFyL9Pw")
            {
                return RedirectToAction("Index", "N1ZppeBQkVXMoMFyL9Pw"); 
            }

            return View(DrinkService.GetUserModel());

        }

        public ActionResult LoadDrinks()
        {
            return View(DrinkService.GetUserModel());
        }
        
        public string BuyDrink(int drinkId, int temp)
        {
            return JsonConvert.SerializeObject(DrinkService.BuyDrink(drinkId, temp),
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
        
    }
}
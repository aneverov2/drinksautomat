﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DrinksAutomat.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DrinksAutomat.Data
{
    public class ApplicationDbContext : DbContext
    {
        //public DbSet<Automat> Automats { get; set; }
        public DbSet<Drink> Drinks { get; set; }
        public DbSet<Coin> Coins { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        //public ApplicationDbContext(string connectionName) :
        //    base(connectionName)
        //{ }
    }
}

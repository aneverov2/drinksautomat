﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DrinksAutomat.Data;
using DrinksAutomat.DAL.Entities;
using DrinksAutomat.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DrinksAutomat.DAL.Repositories
{
    public class DrinkRepository : IRepository<Drink>
    {
        private ApplicationDbContext _context;

        public DrinkRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IQueryable<Drink> GetAll()
        {
            return _context.Drinks;
        }

        public Drink Get(int id)
        {
            return _context.Drinks.Find(id);
        }

        public Drink SingleOrDefault(Func<Drink, bool> predicate)
        {
            return _context.Drinks.SingleOrDefault(predicate);
        }

        public IQueryable<Drink> Find(Expression<Func<Drink, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Create(Drink item)
        {
            _context.Drinks.Add(item);
        }

        public void Update(Drink item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Drink drink = _context.Drinks.Find(id);
            if (drink != null)
                _context.Drinks.Remove(drink);
        }
    }
}

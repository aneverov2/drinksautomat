﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DrinksAutomat.Data;
using DrinksAutomat.DAL.Entities;
using DrinksAutomat.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DrinksAutomat.DAL.Repositories
{
    public class CoinRepository : IRepository<Coin>
    {
        private ApplicationDbContext _context;

        public CoinRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IQueryable<Coin> GetAll()
        {
            return _context.Coins;
        }

        public Coin Get(int id)
        {
            return _context.Coins.Find(id);
        }

        public Coin SingleOrDefault(Func<Coin, bool> predicate)
        {
            return _context.Coins.SingleOrDefault(predicate);
        }

        public IQueryable<Coin> Find(Expression<Func<Coin, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Create(Coin item)
        {
            _context.Coins.Add(item);
        }

        public void Update(Coin item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Coin coin = _context.Coins.Find(id);
            if (coin != null)
                _context.Coins.Remove(coin);
        }
    }
}

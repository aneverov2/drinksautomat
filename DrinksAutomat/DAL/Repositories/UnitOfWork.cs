﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DrinksAutomat.Data;
using DrinksAutomat.DAL.Entities;
using DrinksAutomat.DAL.Interfaces;

namespace DrinksAutomat.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool disposed = false;
        private ApplicationDbContext _context;
        public IRepository<Drink> DrinkRepository { get; }
        public IRepository<Coin> CoinRepository { get; }

        public UnitOfWork(ApplicationDbContext context,
            IRepository<Drink> drinks,
            IRepository<Coin> coins)
        {
            _context = context;
            DrinkRepository = drinks;
            CoinRepository = coins;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}

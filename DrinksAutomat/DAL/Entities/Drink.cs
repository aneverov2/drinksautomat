﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksAutomat.DAL.Entities
{
    public class Drink
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        //public Automat Automat { get; set; }
    }
}

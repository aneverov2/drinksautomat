﻿namespace DrinksAutomat.DAL.Entities
{
    public class Coin
    {
        public int Id { get; set; }
        public int Value { set; get; }
        public int Quantity { set; get; }
        public bool IsBlocked { set; get; }
        //public Automat Automat { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DrinksAutomat.DAL.Entities;
using DrinksAutomat.DAL.Repositories;

namespace DrinksAutomat.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Drink> DrinkRepository { get; }
        IRepository<Coin> CoinRepository { get; }
        void Save();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DrinksAutomat.DAL.Entities;
using Microsoft.AspNetCore.Http;

namespace DrinksAutomat.Models
{
    public class DrinkModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IFormFile ImagePath { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; } 
        //public List<Coin> Coins { get; set; }
    }
}

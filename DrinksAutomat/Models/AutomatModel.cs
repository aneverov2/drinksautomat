﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksAutomat.Models
{
    public class AutomatModel
    {
        public List<CoinModel> Coins { get; set; }
        public List<DrinkModel> Drinks { get; set; }
        public DrinkModel DrinkModel { get; set; }
        public CoinModel CoinModel { get; set; }
        public int Temp { get; set; } // поле для хранения суммы монет 
        public int Remainder { get; set; } 
        public string Notification { get; set; }
    }
}

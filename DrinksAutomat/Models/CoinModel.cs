﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksAutomat.Models
{
    public class CoinModel
    {
        public int Id { get; set; }
        public int Value { set; get; }
        public int Quantity { set; get; }
        public bool IsBlocked { set; get; }
    }
}

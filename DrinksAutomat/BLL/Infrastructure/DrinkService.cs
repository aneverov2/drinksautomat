﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using DrinksAutomat.DAL.Entities;
using DrinksAutomat.DAL.Interfaces;
using DrinksAutomat.Models;
using Microsoft.AspNetCore.Hosting;

namespace DrinksAutomat.BLL.Infrastructure
{
    public class DrinkService
    {
        private readonly IHostingEnvironment _environment;
        private readonly FileUploadService _fileUploadService;
        IUnitOfWork Database { get; set; }
        private CoinService CoinService { get; set; }

        public DrinkService(IHostingEnvironment environment, FileUploadService fileUploadService, IUnitOfWork database, CoinService coinService)
        {
            _environment = environment;
            _fileUploadService = fileUploadService;
            Database = database;
            CoinService = coinService;
        }

        public AutomatModel Get(int id)
        {
            DrinkModel model = Mapper.Map<DrinkModel>(Database.DrinkRepository.Get(id));
            return new AutomatModel()
            {
                DrinkModel = model
            };
        }

        public IEnumerable<DrinkModel> GetAllDrinks()
        {
            return Mapper.Map<IEnumerable<DrinkModel>>(Database.DrinkRepository.GetAll());
        }

        public AutomatModel GetUserModel()
        {
            List<DrinkModel> drinks = GetAllDrinks().ToList();
            List<CoinModel> coins = CoinService.GetAllCoins().ToList();
            AutomatModel model = new AutomatModel()
            {
                Coins = coins,
                Drinks = drinks
            };
            return model;
        }

        public void Create(DrinkModel model)
        {
            string path = Path.Combine(
                _environment.WebRootPath,
                $"images\\{model.Name}\\");
            var imageLogo = $"/images/{model.Name}/{model.ImagePath.FileName}";
            _fileUploadService.Upload(path, model.ImagePath.FileName, model.ImagePath);

            Database.DrinkRepository.Create(new Drink()
            {
                ImagePath = imageLogo,
                Name = model.Name,
                Price = model.Price,
                Quantity = model.Quantity
            });

            Database.Save();
        }

        public void Update(DrinkModel model)
        {
            Drink drink = Database.DrinkRepository.SingleOrDefault(d => d.Id == model.Id);
            if (model.ImagePath != null)
            {
                string path = Path.Combine(
                    _environment.WebRootPath,
                    $"images\\{model.Name}\\");
                var imageLogo = $"/images/{model.Name}/{model.ImagePath.FileName}";
                _fileUploadService.Upload(path, model.ImagePath.FileName, model.ImagePath);
                drink.ImagePath = imageLogo;
            }

            drink.Name = model.Name;
            drink.Price = model.Price;
            drink.Quantity = model.Quantity;

            Database.DrinkRepository.Update(drink);
            Database.Save();
        }

        public void Delete(int id)
        {
            Database.DrinkRepository.Delete(id);
            Database.Save();
        }

        public AutomatModel BuyDrink(int drinkId, int temp)
        {
            Drink drink = Database.DrinkRepository.SingleOrDefault(c => c.Id == drinkId);
            
            if (drink.Price <= temp && drink.Quantity != 0)
            {
                int diff = temp - drink.Price;
                ReturnChange(diff);
                drink.Quantity--;
                Database.Save();
                return new AutomatModel()
                {
                    Remainder = diff,
                    Temp = 0,
                    Notification = string.Format("Ваша сдача: {0}", temp - drink.Price)
                };
            }
            if (drink.Quantity == 0)
            {
                return new AutomatModel()
                {
                    Temp = temp,
                    Notification = "Этот напиток закончился, выберите другой!"
                };
            }
            return new AutomatModel()
            {
                Temp = temp,
                Notification = "Вы не можете купить этот напиток, внесите еще монеты!"
            };
        }

        public void ReturnChange(int diff)
        {
            Coin coin1 = Database.CoinRepository.SingleOrDefault(c => c.Value == 1);
            Coin coin2 = Database.CoinRepository.SingleOrDefault(c => c.Value == 2);
            Coin coin5 = Database.CoinRepository.SingleOrDefault(c => c.Value == 5);

            switch (diff)
            {
                case 1:
                    coin1.Quantity--;
                    Database.Save();
                    break;
                case 2:
                    coin2.Quantity--;
                    Database.Save();
                    break;
                case 3:
                    coin1.Quantity--;
                    coin2.Quantity--;
                    Database.Save();
                    break;
                case 4:
                    coin2.Quantity = coin2.Quantity - 2;
                    Database.Save();
                    break;
                case 5:
                    coin5.Quantity--;
                    Database.Save();
                    break;
                case 6:
                    coin1.Quantity--;
                    coin5.Quantity--;
                    Database.Save();
                    break;
                case 7:
                    coin2.Quantity--;
                    coin5.Quantity--;
                    Database.Save();
                    break;
                case 8:
                    coin1.Quantity--;
                    coin2.Quantity--;
                    coin5.Quantity--;
                    Database.Save();
                    break;
                case 9:
                    coin2.Quantity = coin2.Quantity - 2;
                    coin5.Quantity--;
                    break;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DrinksAutomat.DAL.Entities;
using DrinksAutomat.DAL.Interfaces;
using DrinksAutomat.Models;

namespace DrinksAutomat.BLL.Infrastructure
{
    public class CoinService
    {
        IUnitOfWork Database { get; set; }

        public CoinService(IUnitOfWork database)
        {
            Database = database;
        }

        public AutomatModel Get(int id)
        {
            CoinModel model = Mapper.Map<CoinModel>(Database.CoinRepository.Get(id));
            return new AutomatModel()
            {
                CoinModel = model
            };
        }

        public IEnumerable<CoinModel> GetAllCoins()
        {
            return Mapper.Map<IEnumerable<CoinModel>>(Database.CoinRepository.GetAll());
        }

        public void Create(CoinModel model)
        {
            Database.CoinRepository.Create(Mapper.Map<Coin>(model));
            Database.Save();
        }

        public void Update(CoinModel model)
        {
            Database.CoinRepository.Update(Mapper.Map<Coin>(model));
            Database.Save();
        }

        public void Delete(int id)
        {
            Database.CoinRepository.Delete(id);
            Database.Save();
        }
        
        public AutomatModel GetCoinValue(int value, int temp)
        {
            Coin coin = Database.CoinRepository.SingleOrDefault(c => c.Value == value);
            coin.Quantity++;
            Database.Save();

            return new AutomatModel()
            {
                Temp = value + temp
            }; 
        }
    }
}
